IF OBJECT_ID('Appointments', 'U') IS NOT NULL
    DROP TABLE Appointments;
IF OBJECT_ID('Rooms', 'U') IS NOT NULL
    DROP TABLE Rooms;
IF OBJECT_ID('Branches', 'U') IS NOT NULL
    DROP TABLE Branches;
IF OBJECT_ID('Therapies', 'U') IS NOT NULL
    DROP TABLE Therapies;
IF OBJECT_ID('Clients', 'U') IS NOT NULL
    DROP TABLE Clients;
IF OBJECT_ID('Doctors', 'U') IS NOT NULL
    DROP TABLE Doctors;

CREATE TABLE Doctors(
  ID smallint IDENTITY(1, 1) PRIMARY KEY,
  Name varchar(255),
  Position varchar(50),
  Specialization varchar(255),
  Gender char(1)
);

CREATE TABLE Clients(
  ID int IDENTITY(1, 1) PRIMARY KEY,
  Name varchar(255),
  Marital_status char(1),
  Gender char(1),
  Standard_of_living varchar(20)
);

CREATE TABLE Therapies(
  ID int IDENTITY(1, 1) PRIMARY KEY,
  Entry_description varchar(8000),
  Goal varchar(1000),
  End_information varchar(50),
  Date_start datetime
);

CREATE TABLE Branches(
  Nr smallint IDENTITY(1, 1) PRIMARY KEY,
  Address varchar(255)
);

CREATE TABLE Rooms(
  ID smallint IDENTITY(1, 1) PRIMARY KEY,
  Branches_nr smallint FOREIGN KEY REFERENCES Branches(Nr),
  Walls_color varchar(50),
  Has_window bit,
  Has_AC bit,
  Style varchar(255)
);

CREATE TABLE Appointments(
  ID int IDENTITY(1, 1) PRIMARY KEY,
  Date_time datetime,
  Duration smallint,
  Absent_doctor bit,
  Absent_patient bit,
  Doctors_ID smallint FOREIGN KEY REFERENCES Doctors(ID),
  Rooms_ID smallint FOREIGN KEY REFERENCES Rooms(ID),
  Clients_ID int FOREIGN KEY REFERENCES Clients(ID),
  Therapies_ID int FOREIGN KEY REFERENCES Therapies(ID)
);
