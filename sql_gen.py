import logging

import data


def pack_sql_value(value):
    if str(type(value)).find("str") != -1:
        return "\'" + value + "\'"
    elif str(type(value)).find("datetime") != -1:
        return "\'" + str(value) + "\'"
    else:
        return "{}".format(value)


class SqlInserter:
    debug_mode = False
    rows_limit = 1000

    def __init__(self, table, columns=None):
        self._current_row_count = 0
        self._query = ""
        self._prefix_exists = False
        self._table = table
        self._columns = columns
        if self.debug_mode:
            self._space = ' '
            self._indent = "  "
        else:
            self._space = ''
            self._indent = ''

    def insert(self, values):
        if self._current_row_count >= self.rows_limit:
            self._split()
            self._current_row_count = 1
        else:
            self._current_row_count += 1
        self._insert_row(values)

    def get_query(self):
        if self._prefix_exists:
            self._split()
        return self._query

    def clear(self):
        self._current_row_count = 0
        self._query = ""
        self._prefix_exists = False

    def _format_row(self, values):
        row = "("
        for value in values:
            row += pack_sql_value(value) + ",{}".format(self._space)
        if self.debug_mode:
            row = row[:-2]  # remove that trailing ,_
        else:
            row = row[:-1]  # remove that trailing ,
        row += ")"
        return row

    def _insert_prefix(self):
        self._query += "INSERT INTO "
        self._query += self._table
        if self._columns is not None:
            self._query += self._format_row(self._columns).replace("\'", "")
        self._query += " VALUES\n"

    def _insert_row(self, values):
        if not self._prefix_exists:
            self._insert_prefix()
            self._prefix_exists = True
        else:
            self._query += ",\n"
        self._query += self._indent + self._format_row(values)

    def _split(self):
        self._query += ";\n"
        self._prefix_exists = False


# Example
# sql_inserter = SqlInserter("the_table", ["A", "B"])
# sql_inserter.rows_limit = 2
# sql_inserter.insert([1, '2'])
# sql_inserter.insert([2, '4'])
# sql_inserter.insert([4, '6'])
# print(sql_inserter.get_query())


class SqlWriter:
    def __init__(self, simulation: data.Simulation):
        self._simulation = simulation

    def write_all(self, file):
        logging.info("Writing doctors...")
        self.write_doctors(file)
        logging.info("Writing patients/clients...")
        self.write_patients(file)
        logging.info("Writing therapies...")
        self.write_therapies(file)
        logging.info("Writing branches...")
        self.write_branches(file)
        logging.info("Writing rooms...")
        self.write_rooms(file)
        logging.info("Writing appointments...")
        self.write_appointments(file)

    def write_doctors(self, file):
        inserter = SqlInserter("Doctors", ["Name", "Position", "Specialization", "Gender"])
        for doctor in self._simulation.doctors:
            inserter.insert([doctor.name, doctor.position, doctor.specialization, doctor.gender])
        file.write(inserter.get_query())
        file.write('\n')

    def write_patients(self, file):
        inserter = SqlInserter("Clients", ["Name", "Marital_status", "Gender", "Standard_of_living"])
        nr_of_patients = len(self._simulation.patients)
        current_index = 0
        for patient in self._simulation.patients:
            logging.info("Patients/clients writing completion: {}%".format(current_index / nr_of_patients * 100))
            inserter.insert([patient.name, patient.marital_status, patient.gender, patient.standard])
            current_index += 1
        file.write(inserter.get_query())
        file.write('\n')

    def write_therapies(self, file):
        inserter = SqlInserter("Therapies", ["Entry_description", "Goal", "End_information", "Date_start"])
        nr_of_therapies = len(self._simulation.therapies)
        current_index = 0
        for therapy in self._simulation.therapies:
            logging.info("Therapies writing completion: {}%".format(current_index / nr_of_therapies * 100))
            inserter.insert([therapy.description, therapy.goal, therapy.status, therapy.first_appointment_datetime])
            current_index += 1
        file.write(inserter.get_query())
        file.write('\n')

    def write_branches(self, file):
        inserter = SqlInserter("Branches", ["Address"])
        for branch in self._simulation.branches:
            inserter.insert([branch.address])
        file.write(inserter.get_query())
        file.write('\n')

    def write_rooms(self, file):
        inserter = SqlInserter("Rooms", ["Branches_nr", "Walls_color", "Has_window", "Has_AC", "Style"])
        for room in self._simulation.rooms:
            inserter.insert([room.branch_index+1, room.wall_color, int(room.has_window), int(room.has_ac), room.style])
        file.write(inserter.get_query())
        file.write('\n')

    def write_appointments(self, file):
        inserter = SqlInserter("Appointments", ["Date_time", "Duration", "Absent_doctor", "Absent_patient",
                                                "Doctors_ID", "Rooms_ID", "Clients_ID", "Therapies_ID"])
        nr_of_appointments = len(self._simulation.appointments)
        current_index = 0
        for appointment in self._simulation.appointments:
            logging.info("Appointments writing completion: {}%".format(current_index / nr_of_appointments * 100))
            inserter.insert([appointment.date_time, appointment.duration,
                             appointment.absent_doctor, appointment.absent_patient,
                             self._simulation.therapies[appointment.therapy_index].doctor_index+1,
                             appointment.room_index+1,
                             self._simulation.therapies[appointment.therapy_index].patient_index+1,
                             appointment.therapy_index+1])
            current_index += 1
            if current_index % 50000 == 0:
                file.write(inserter.get_query())
                inserter.clear()
        last_query = inserter.get_query()
        if last_query != "":
            file.write(last_query)
        file.write('\n')


class SqlUpdater:
    def __init__(self, table, column):
        self._query = "UPDATE dbo.{}\n".format(table)
        self._query += "  SET {}=".format(column)

    def get_query(self, value, condition_key, condition_value):
        query = self._query
        query += pack_sql_value(value)
        query += " WHERE {}=".format(condition_key)
        query += pack_sql_value(condition_value)
        query += ";\n"
        return query
