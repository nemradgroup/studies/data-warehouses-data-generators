import datetime
import logging

import math
from numpy import random


class Person:
    def __init__(self):
        self.gender = self._get_random_gender()
        self.name = self._get_random_fname() + ' ' + self._get_random_sname()

    @staticmethod
    def _get_random_gender():
        return random.choice(['M', 'F', 'O'], p=[.45, .5, .05])

    @staticmethod
    def _get_first_names_dictionary():
        male_names = ["Andrzej", "Marcin", "Piotr", "Krystian", "Ryszard", "Zbigniew", "Alojzy", "Marek", "Robert",
                      "Bogumil", "Mateusz", "Radzymil", "Janusz", "Swietopelk", "Sebastian", "Kasper", "Korneliusz",
                      "Filip", "Eustachy", "Lalit", "Jan", "Kuba", "Marcel", "Maxymilian", "John", "Bernardyn",
                      "Milosz", "Arcadio", "Aureliusz", "Ronald", "Mariusz", "Protazy", "Antoni", "Franciszek",
                      "Lukasz", "Damian", "Aleksander", "Mikolaj", "Dominik", "Bartosz", "Bartlomiej"]
        female_names = ["Aneta", "Anna", "Magdalena", "Lena", "Julia", "Joanna", "Izabela", "Patrycja", "Monika",
                        "Grazyna", "Ewa", "Karolina", "Urszula", "Remedios", "Rebeka", "Hermiona", "Luna", "Ginny",
                        "Lily", "Amaranta", "Regina", "Aniela", "Stanislawa", "Marcelina", "Wladyslawa", "Angela",
                        "Klaudia", "Kornelia", "Kinga", "Michalina", "Agnieszka", "Malgorzata", "Faustyna", "Zdzislawa",
                        "Kunekunda", "Toshiba", "Mikasa"]
        return {'M': male_names, 'F': female_names, 'O': male_names + female_names}

    @staticmethod
    def _generate_multiname(names, amount_chances, separator):
        amount_nrs = [i for i in range(1, len(amount_chances) + 1)]
        amount = random.choice(amount_nrs, p=amount_chances)
        chosen_names = random.choice(names, amount, False)
        return separator.join(chosen_names)

    def _get_random_fname(self):
        amount_chances = [0, .35, .05, .005]  # chances that someone will have 1, 2, 3, 4 first names
        amount_chances[0] = 1 - sum(amount_chances)
        names_dict = self._get_first_names_dictionary()
        return self._generate_multiname(names_dict[self.gender], amount_chances, ' ')

    @staticmethod
    def _get_second_names_dictionary():
        # gender-dependent are snames that end with -a in case of a female and -i if male
        gender_dependent_names = ["Kowalsk", "Jaworsk", "Imielsk", "Mikulsk", "Jackowsk", "Kochansk"]
        gender_independent_names = ["Karalus", "Bodziwiec", "Matorzek", "Sobek", "Rabek", "Mysz", "Ryba", "Drzewo",
                                    "Manus", "Jednorozec", "Juszczyk", "Konopka", "Konopek", "Kopytko", "Jez",
                                    "Racuch", "Kamien", "Kowalczyk", "Domek", "Szatan", "Jarzabek", "Andrzejczyk",
                                    "Pogorzelczyk", "Lukasiewicz", "Serce", "Batory", "Kolodziejczyk", "Misiewicz",
                                    "Roza", "Marciniuk", "Lenkiewicz", "Letkiewicz"]
        male_names = [x + 'i' for x in gender_dependent_names] + gender_independent_names
        female_names = [x + 'a' for x in gender_dependent_names] + gender_independent_names
        return {'M': male_names, 'F': female_names, 'O': male_names + female_names}

    def _get_random_sname(self):
        gender_coefficient = {'M': 0.1, 'F': 1, 'O': .5}[self.gender]
        raw_amount_chances = [0, .15]  # chances that someone will have 1, 2 second names
        amount_chances = [raw*gender_coefficient for raw in raw_amount_chances]
        amount_chances[0] = 1 - sum(amount_chances)
        names_dict = self._get_second_names_dictionary()
        return self._generate_multiname(names_dict[self.gender], amount_chances, '-')


class Patient(Person):
    def __init__(self):
        super().__init__()
        self.marital_status = self._get_random_marital_status()
        self.standard = self._get_random_standard_of_living()

    @staticmethod
    def _get_random_marital_status():
        return random.choice(['S', 'M', 'D', 'W', 'O'], p=[.40, .25, .10, .20, .05])

    @staticmethod
    def _get_random_standard_of_living():
        return random.choice(["Poor", "Mediocre", "Normal", "Good", "Wonderful"], p=[.05, .10, .55, .20, .1])


class GradesSet:
    def __init__(self):
        self.grades = {}

    def randomize(self):  # grades are massively not realistically generated
        for grade_name in self.grades:
            self.grades[grade_name] = round(random.uniform(1, 5) + random.binomial(5, .4))

    def get_cumulative_grade(self):
        grade_names = list(self.grades)
        max = len(grade_names) * 10
        current = 0
        for grade_name in grade_names:
            current += self.grades[grade_name]
        return round(current/max, 2)


class DoctorGradesSet(GradesSet):
    def __init__(self):
        super().__init__()
        self.grades['1. attendance'] = 0
        self.grades['2. dutifulness'] = 0
        self.grades['3. involvement'] = 0
        for i in range(5):
            self.grades['{}. patient_{}'.format(4+i, i + 1)] = 0
        self.grades['9. coworkers'] = 0


class Doctor(Person):
    def __init__(self, start_month_for_grades: datetime.date):
        super().__init__()
        self.position = self._get_random_position()
        self.specialization = self._get_random_specialization()
        self.grades = self._get_random_grades(start_month_for_grades)

    @staticmethod
    def _get_random_position():
        return random.choice(["Psychologist", "Psychotherapist"], p=[.25, .75])

    @staticmethod
    def _get_random_specialization():
        return random.choice(["Family problems", "Addictions", "Depression", "Tangible losses", "Intangible losses"])

    @staticmethod
    def _get_random_grades(start_month: datetime.date):
        grades = {}
        current_month = start_month
        today = datetime.date.today()
        while current_month.year < today.year or current_month.month < today.month:
            new_grades = DoctorGradesSet()
            new_grades.randomize()
            grades[current_month.strftime('%Y-%m')] = new_grades
            if current_month.month == 12:
                current_month = current_month.replace(year=current_month.year+1, month=1)
            else:
                current_month = current_month.replace(month=current_month.month+1)
        return grades


class Branch:
    def __init__(self):
        self.address = "{} ul. {} {}".format(self._get_random_city(), self._get_random_street(), self._get_random_nr())

    @staticmethod
    def _get_random_city():
        return random.choice(["Gdynia", "Gdansk", "Sopot"])

    @staticmethod
    def _get_random_street():
        return random.choice(["Morska", "Dworcowa", "Powstancow", "Wojska polskiego", "Zlota"])

    @staticmethod
    def _get_random_nr():
        house_nr = str(round(random.uniform(0, 60) + random.binomial(200, .1)))
        if random.uniform() < .3:
            return house_nr
        return house_nr + '/' + str(round(random.uniform(0, 20) + random.binomial(50, .4)))


class Room:
    def __init__(self, branch_index: int):
        self.wall_color = self._get_random_wall_color()
        self.has_window = bool(round(random.uniform(0, 1)))
        self.has_ac = bool(round(random.uniform(0, 1)))
        self.style = self._get_random_style()
        self.branch_index = branch_index

    @staticmethod
    def _get_random_wall_color():
        return random.choice(["Green", "Blue", "Cyan", "Yellow", "White", "Gray"])

    @staticmethod
    def _get_random_style():
        return random.choice(["Modern", "Traditional", "Modern minimalistic", "Old school"])


class Therapy:
    def __init__(self, doctor_index: int, patient_index: int, duration_coefficient: float):
        self.goal = self._get_random_goal()
        self.description = self._get_random_description()
        self.status = self._get_random_status(duration_coefficient)
        self.doctor_index = doctor_index
        self.patient_index = patient_index
        self.first_appointment_datetime = None  # to be entered on first associated appointment

    @staticmethod
    def _get_random_goal():
        return "yadda" * round(random.uniform(1, 2))

    @staticmethod
    def _get_random_description():
        return "yadda" * round(random.uniform(2, 3))

    @staticmethod
    def _get_random_status(duration_coefficient: float):
        # duration coefficient-related calculations make the status more real, e.g. therapy that started 10 years
        # ago is more bound to have already ended than a therapy that started yesterday; <0;1>, higher => older therapy
        # example: value of .75 means that half of whole probability of not_complete will transfer to complete
        not_complete, complete = [.3, .05], [.5, .15]  # original probs
        nc_c_ratio, c_nc_ratio = sum(not_complete) / sum(complete), sum(complete) / sum(not_complete)
        if duration_coefficient > .5:
            nc_multiplier = 1 - (duration_coefficient - .5) * 2
            c_multiplier = 1 + (duration_coefficient - .5) * 2 * nc_c_ratio
        elif duration_coefficient < .5:
            nc_multiplier = 1 + (.5 - duration_coefficient) * 2 * c_nc_ratio
            c_multiplier = 1 - (.5 - duration_coefficient) * 2
        else:
            nc_multiplier, c_multiplier = 1, 1
        probs = [x * nc_multiplier for x in not_complete] + [x * c_multiplier for x in complete]
        return random.choice(["In progress", "Halted", "Successful", "Abandoned"], p=probs)

    def know_appointment_datetime(self, datetime):
        if self.first_appointment_datetime is None:
            self.first_appointment_datetime = datetime


class AppointmentGradesSet(GradesSet):
    def __init__(self):
        super().__init__()
        self.grades['1. homework'] = 0
        self.grades['2. recent_problems'] = 0
        self.grades['3. current_mental_health'] = 0
        self.grades['4. progress'] = 0
        self.grades['5. curement_level'] = 0


class Appointment:
    def __init__(self, date_time: datetime.datetime, therapy_index: int, room_index: int):
        self.date_time = date_time
        self.duration = self._get_random_duration()
        self.absent_doctor = self._get_random_doctor_absence()
        self.absent_patient = self._get_random_patient_absence()
        self.therapy_index = therapy_index
        self.room_index = room_index
        self.grades = self._get_random_grades()
        self.notes = self._get_random_notes()

    @staticmethod
    def _get_random_duration():
        return round(40 - random.binomial(10, .05))

    @staticmethod
    def _get_random_doctor_absence():
        return round(1 - random.binomial(1, .95))

    @staticmethod
    def _get_random_patient_absence():
        return round(1 - random.binomial(1, .8))

    def _get_random_grades(self):
        grades = None
        if not (self.absent_doctor or self.absent_patient):  # appointment really happened
            grades = AppointmentGradesSet()
            grades.randomize()
        return grades

    @staticmethod
    def _get_random_notes():
        return "yadda" * round(random.uniform(2, 7))


class Simulation:
    nr_of_branches = 3
    nr_of_rooms_per_branch = 4
    time0_at_workdays = datetime.time(7, 30)
    timee_at_workdays = datetime.time(17, 0)
    duration_of_meeting = datetime.time(0, 40)
    required_nr_of_meetings = 1001000
    mean_meetings_per_patient = 20

    def __init__(self):
        self.doctors = []
        self.patients = []
        self.therapies = []
        self.branches = []
        self.rooms = []
        self.appointments = []

    def generate(self):
        logging.info("Starting internal generation...")
        logging.info("Generating doctors...")
        self._generate_doctors()
        logging.info("Generating patients...")
        self._generate_patients()
        logging.info("Generating therapies...")
        self._generate_therapies()
        logging.info("Generating branches...")
        self._generate_branches()
        logging.info("Generating rooms...")
        self._generate_rooms()
        logging.info("Generating appointments...")
        self._generate_appointments()
        logging.info("Internal generation complete")

    def _generate_doctors(self):
        nr_of_needed_doctors = self.nr_of_branches * self.nr_of_rooms_per_branch

        for i in range(nr_of_needed_doctors):
            self.doctors += [Doctor(datetime.date(2000, 1, 1))]

    def _generate_patients(self):
        # as for now just assume each patient got equal amount of meetings and one therapy
        nr_of_patients = math.ceil(self.required_nr_of_meetings / self.mean_meetings_per_patient)

        for i in range(nr_of_patients):
            logging.info("Patients generation completion: {}%".format(i / nr_of_patients * 100))
            self.patients += [Patient()]

    def _generate_therapies(self):
        nr_of_therapies = len(self.patients)  # simplified model - one therapy per patient
        current_doctor_index = 0
        for i in range(nr_of_therapies):
            logging.info("Therapies generation completion: {}%".format(i / nr_of_therapies * 100))
            self.therapies += [Therapy(current_doctor_index, i, 1 - i/nr_of_therapies)]
            current_doctor_index = (current_doctor_index + 1) % len(self.doctors)

    def _generate_branches(self):
        for i in range(self.nr_of_branches):
            self.branches += [Branch()]

    def _generate_rooms(self):
        for i in range(self.nr_of_branches):
            for j in range(self.nr_of_rooms_per_branch):
                self.rooms += [Room(i)]

    def _generate_appointments(self):
        meeting_duration = datetime.timedelta(minutes=self.duration_of_meeting.minute)

        def find_earlier_datetime(current_date: datetime.datetime):
            if (current_date - meeting_duration).time() < self.time0_at_workdays:
                if current_date.weekday() == 0:
                    days_delta = 3
                else:
                    days_delta = 1
                day_before = current_date - datetime.timedelta(days=days_delta)
                hour_then = self.timee_at_workdays.hour - 1
                minute_then = 60 + self.timee_at_workdays.minute - self.duration_of_meeting.minute
                return day_before.replace(hour=hour_then, minute=minute_then)
            return current_date - meeting_duration

        last_datetime = datetime.datetime(2019, 3, 20, self.timee_at_workdays.hour - 1, 60 + self.timee_at_workdays.minute - self.duration_of_meeting.minute)
        remaining_nr_of_meetings = self.required_nr_of_meetings
        current_room_index = 0
        total_nr_of_therapies = len(self.patients)  # each patient has 1 and only 1 therapy associated with them
        total_nr_of_rooms = len(self.rooms)

        therapies_selection_span = 50
        selectable_therapies = {}
        busy_doctors = []

        def get_random_nr_of_meetings():
            spread = .5
            min_nr = self.mean_meetings_per_patient * (1-spread)
            max_nr = self.mean_meetings_per_patient * (1+spread)
            return round(random.uniform(min_nr, max_nr))

        for i in range(total_nr_of_therapies - therapies_selection_span, total_nr_of_therapies):
            selectable_therapies[i] = get_random_nr_of_meetings()
        nr_of_already_selectable_therapies = therapies_selection_span

        def get_random_therapy(therapies):
            nonlocal selectable_therapies
            nonlocal busy_doctors

            possible_indices = []
            for index in list(selectable_therapies):
                if therapies[index].doctor_index not in busy_doctors:
                    possible_indices += [index]

            if possible_indices:
                return random.choice(possible_indices)
            else:
                return -1

        def go_back_in_time():
            nonlocal last_datetime
            nonlocal busy_doctors

            last_datetime = find_earlier_datetime(last_datetime)
            busy_doctors = []

        def switch_room():
            nonlocal current_room_index
            nonlocal last_datetime

            current_room_index = (current_room_index + 1) % total_nr_of_rooms
            if current_room_index == 0:
                go_back_in_time()

        while remaining_nr_of_meetings > 0 and selectable_therapies:
            logging.info("Appointments generation completion: {}%".format(100 - remaining_nr_of_meetings/self.required_nr_of_meetings * 100))

            chosen_therapy_index = get_random_therapy(self.therapies)
            if chosen_therapy_index == -1:
                go_back_in_time()
                continue

            self.appointments += [Appointment(last_datetime, chosen_therapy_index, current_room_index)]
            self.therapies[chosen_therapy_index].know_appointment_datetime(last_datetime)
            busy_doctors += [self.therapies[chosen_therapy_index].doctor_index]

            if selectable_therapies[chosen_therapy_index] == 1:
                del selectable_therapies[chosen_therapy_index]
                if nr_of_already_selectable_therapies < total_nr_of_therapies:
                    selectable_therapies[total_nr_of_therapies - nr_of_already_selectable_therapies - 1] = get_random_nr_of_meetings()
                nr_of_already_selectable_therapies += 1
            else:
                selectable_therapies[chosen_therapy_index] -= 1

            switch_room()

            remaining_nr_of_meetings -= 1

        # finish the job in empty therapies:
        # place in each one of them a single appointment to avoid too much additional work
        while selectable_therapies:
            chosen_therapy_index = get_random_therapy(self.therapies)
            if chosen_therapy_index == -1:
                go_back_in_time()
                continue

            self.appointments += [Appointment(last_datetime, chosen_therapy_index, current_room_index)]
            self.therapies[chosen_therapy_index].know_appointment_datetime(last_datetime)
            busy_doctors += [self.therapies[chosen_therapy_index].doctor_index]

            del selectable_therapies[chosen_therapy_index]
            if nr_of_already_selectable_therapies < total_nr_of_therapies:
                selectable_therapies[total_nr_of_therapies - nr_of_already_selectable_therapies - 1] = 1
            nr_of_already_selectable_therapies += 1

            current_room_index = (current_room_index + 1) % total_nr_of_rooms
            switch_room()

        self.appointments.reverse()
