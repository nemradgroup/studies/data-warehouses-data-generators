from copy import deepcopy
import logging
import os
import time

logging.getLogger().setLevel("INFO")

import data
import csv_gen
import sql_gen

try:
    os.mkdir("./out")
except OSError:
    pass

logging.info("Generating data for database...")
time_start = time.time()

# obtain data
simulation = data.Simulation()
simulation.generate()

# before writing each SQL version, we'll need to divide data into 2 versions:
# - one where no therapies are considered totally abandoned and there's only half of all appointments
# - other one where therapies are in their original form and appointments are added up to the last one

# prepare t0 version of data
logging.info("Preparing t0 version...")
data_t0 = deepcopy(simulation)
data_t0.appointments = simulation.appointments[:len(simulation.appointments)*1000//1001]
for i in range(len(simulation.therapies)):
    if simulation.therapies[i].status == "Abandoned":
        data_t0.therapies[i].status = "Halted"

# write first SQL version
sql_writer_t0 = sql_gen.SqlWriter(data_t0)
logging.info("Writing first file...")
with open("./out/inserts.sql", "w") as file:
    sql_writer_t0.write_all(file)

# prepare t1 version of data
logging.info("Preparing t1 version...")
data_t1 = deepcopy(simulation)
data_t1.appointments = simulation.appointments[len(simulation.appointments)*1000//1001:]

# write second SQL version
logging.info("Writing second file...")
with open("./out/updates.sql", "w") as file:
    sql_writer_t1 = sql_gen.SqlWriter(data_t1)
    sql_writer_t1.write_appointments(file)
    sql_updater = sql_gen.SqlUpdater("Therapies", "End_information")
    for i in range(len(simulation.therapies)):
        if simulation.therapies[i].status == "Abandoned":
            file.write(sql_updater.get_query("Abandoned", "ID", i) + '\n')
    file.write('\n')

# write both excel sheets (csv)
csv_writer_t0 = csv_gen.CSVWriter(data_t0)
csv_writer_t0.write("./out/sheet1.csv", 1)
csv_writer_t0.write("./out/sheet2.csv", 2)

csv_writer_t1 = csv_gen.CSVWriter(data_t1)
csv_writer_t1.write("./out/sheet1_updated.csv", 1)

logging.info("Task finished in {:.3f} s".format(time.time() - time_start))
