import logging

import data


# inserter that collects records about an entity and creates a 3d structure from it, having also nulls, written in 2d
class CSV3DRowInserter():
    def __init__(self):
        self._content = []

    # value in insert is a 2d array, where the outer array corresponds to columns and the inner to rows
    def insert(self, value):
        self._content += [value]

    def get_nr_of_columns(self):
        return len(self._content)

    def get_row(self):
        complex_row = ""
        lenghts_of_columns = []
        for column in self._content:
            lenghts_of_columns += [len(column)]
        for current_subrow in range(max(lenghts_of_columns)):
            for column_nr in range(len(self._content)):
                if lenghts_of_columns[column_nr] > current_subrow:
                    complex_row += "{};".format(self._content[column_nr][current_subrow])
                else:
                    complex_row += ';'
            complex_row = complex_row[:-1] + '\n'
        return complex_row


class CSVWriter:
    write_3d = False

    def __init__(self, simulation: data.Simulation):
        self._simulation = simulation

    def write(self, file_path: str, sheet: int):
        with open(file_path, "w") as file:
            if sheet == 1:
                self._write_sheet_1(file)
            elif sheet == 2:
                self._write_sheet_2(file)

    def _write_sheet_1(self, file):
        therapies_inserters = []
        nr_of_appointments = len(self._simulation.appointments)
        max_columns = 0

        for _ in self._simulation.therapies:
            therapies_inserters += [None if self.write_3d else ""]

        for appointment_index in range(len(self._simulation.appointments)):
            appointment = self._simulation.appointments[appointment_index]
            logging.info("Appointments grades rows completion: {}%".format(appointment_index / nr_of_appointments * 100))

            # stop processing this appointment now if it didn't happen
            if appointment.grades is None:
                continue

            therapy_index = appointment.therapy_index

            if therapies_inserters[therapy_index] is None or not self.write_3d:
                patient = self._simulation.patients[self._simulation.therapies[therapy_index].patient_index]
                doctor = self._simulation.doctors[self._simulation.therapies[therapy_index].doctor_index]

                if self.write_3d:
                    therapies_inserters[therapy_index] = CSV3DRowInserter()
                    therapies_inserters[therapy_index].insert([self._simulation.therapies[therapy_index].patient_index + 1])
                    therapies_inserters[therapy_index].insert([patient.name])
                    therapies_inserters[therapy_index].insert([self._simulation.therapies[therapy_index].doctor_index + 1])
                    therapies_inserters[therapy_index].insert([doctor.name])
                else:
                    therapies_inserters[therapy_index] += "{};{};{};{};".format(
                        self._simulation.therapies[therapy_index].patient_index + 1,
                        patient.name,
                        self._simulation.therapies[therapy_index].doctor_index + 1,
                        doctor.name
                    )

            if self.write_3d:
                entry = [str(appointment.date_time)]

                for grade_name in appointment.grades.grades:
                    entry += [str(appointment.grades.grades[grade_name])]

                therapies_inserters[therapy_index].insert(entry)
            else:
                therapies_inserters[therapy_index] += "{};".format(appointment.date_time)

                for grade_name in appointment.grades.grades:
                    therapies_inserters[therapy_index] += str(appointment.grades.grades[grade_name]) + ';'

                therapies_inserters[therapy_index] = therapies_inserters[therapy_index][:-1] + '\n'

        if self.write_3d:
            therapies_inserters[:] = [value for value in therapies_inserters if value is not None and value.get_nr_of_columns() > 2]

            for inserter in therapies_inserters:
                max_columns = max(max_columns, inserter.get_nr_of_columns())

            for inserter in therapies_inserters:
                while inserter.get_nr_of_columns() < max_columns:
                    inserter.insert([])

            file.write("Patient\'s name;Doctor\'s name")
            for i in range(max_columns-3):
                file.write(";Appointment #{}".format(i+1))
            file.write('\n')

            nr_of_therapies = len(therapies_inserters)

            for i in range(nr_of_therapies):
                logging.info("Appointments grades sheet writing completion: {}%".format(i/nr_of_therapies * 100))
                file.write(therapies_inserters[i].get_row())
        else:
            therapies_inserters[:] = [value for value in therapies_inserters if value is not ""]

            file.write("Patient\'s ID;Patient\'s name;Doctor\'s ID;Doctor\'s name;Appointment datetime")
            for grade_name in data.AppointmentGradesSet().grades:
                file.write(";{}".format(grade_name))
            file.write('\n')

            nr_of_therapies = len(therapies_inserters)

            for i in range(nr_of_therapies):
                logging.info("Appointments grades sheet writing completion: {}%".format(i / nr_of_therapies * 100))
                file.write(therapies_inserters[i])

    def _write_sheet_2(self, file):
        if self.write_3d:
            headers = "Employee\'s ID;Employee\'s name"
        else:
            headers = "Employee\'s ID;Employee\'s name;Grades date"
        for grade_name in data.DoctorGradesSet().grades:
            headers += ";{}".format(grade_name)
        headers += ";Cumulative grade"
        file.write(headers + '\n')

        for doctor_id in range(len(self._simulation.doctors)):
            doctor = self._simulation.doctors[doctor_id]

            if self.write_3d:
                inserter = CSV3DRowInserter()
                inserter.insert([doctor_id + 1])
                inserter.insert([doctor.name])
                for grades_set_date in doctor.grades:
                    entry = []
                    grades_set = doctor.grades[grades_set_date]
                    for grade_name in grades_set.grades:
                        entry += [grades_set.grades[grade_name]]
                    inserter.insert(entry + [grades_set.get_cumulative_grade()])
                file.write(inserter.get_row())
            else:
                inserter = ""
                for grades_set_date in doctor.grades:
                    inserter += "{};{};{}".format(str(doctor_id + 1), doctor.name, str(grades_set_date))
                    grades_set = doctor.grades[grades_set_date]
                    for grade_name in grades_set.grades:
                        inserter += ";{}".format(grades_set.grades[grade_name])
                    inserter += ";{}\n".format(grades_set.get_cumulative_grade())
                file.write(inserter)
